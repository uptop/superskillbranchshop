package com.skillbranch.bestshop.mvp.models;

import com.skillbranch.bestshop.data.storage.realm.ProductRealm;

import io.realm.RealmResults;

/**
 * Created by pashko00710 on 12.03.17.
 */

public class FavoriteModel extends AbstractModel {
    public RealmResults<ProductRealm> getAllFavorites() {
        return mDataManager.getAllFavoriteProducts();
    }
}
