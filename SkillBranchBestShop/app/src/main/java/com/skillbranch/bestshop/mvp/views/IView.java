package com.skillbranch.bestshop.mvp.views;

public interface IView {
    boolean viewOnBackPressed();
}
