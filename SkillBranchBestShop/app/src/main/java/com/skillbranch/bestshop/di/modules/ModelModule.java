package com.skillbranch.bestshop.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.skillbranch.bestshop.data.managers.DataManager;

@Module
public class ModelModule extends FlavorModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return DataManager.getInstance();
    }
}
