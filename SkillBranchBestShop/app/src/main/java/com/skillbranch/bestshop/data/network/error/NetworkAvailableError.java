package com.skillbranch.bestshop.data.network.error;

public class NetworkAvailableError extends Throwable {
    public NetworkAvailableError() {
        super("Интернет недоступен, попробуйте позже");
    }
}
